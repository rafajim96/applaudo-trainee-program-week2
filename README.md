# Setup #

First you have to clone the project, all the .rb files for this project are on the develop branch, the file with all the methods is on lib/week2.rb

To run this project you just need to call the method: read_and_validate_file()

### Method's used ###

* ### write_date_csv(date_array, csv_file_name) ###

Receives: Receives an array with the data that we want to write and the name of the file that we want to give to the CSV file.

What it does: Writes a CSV file with the received data.

Returns: Nothing.

Throws: A generic Exception in case that there is one when writing the file.

* ### print_csv_results(valid_dates, invalid_dates) ###

Receives: Receives two arrays, one that contains the valid dates and the other contains the invalid dates.

What it does: Prints a console-graphic that represents the results of the validated CSV file.

Returns: Nothing.

* ### validate_not_negative_datetime(date) ###

Receives: Receives a string that represents a date-time with the format {dd/mm/yyyy hh:mm:ss}.

What it does: Validates if the date and time are not negative and the parse's the String to a Time object with EST timezone.

Returns: A Time object in case that the input string is not negative.

Throws: An ArgumentError exception if the date-time is negative or the .parse method fails to parse the String to Time.

